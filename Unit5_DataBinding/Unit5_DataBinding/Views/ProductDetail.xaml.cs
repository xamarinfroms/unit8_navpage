﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit5_DataBinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductDetail : ContentPage
    {
    

        public ProductDetail()
        {
            InitializeComponent();
        }

        public ProductDetail(ProductViewModel viewModel)
        {
            this.BindingContext = viewModel;
            this.InitializeComponent();
           
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await this.Navigation.PopAsync();
        }
    }
}
